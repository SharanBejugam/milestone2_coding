﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using Sharan_Milestone2_CodingTest_09_09_02_55.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Commands.PlaceOrderCommand
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {
        private readonly IGroceryService _data;

        public PlaceOrderHandler(IGroceryService data)
        {
            _data = data;
        }

        public async Task<ProductOrder> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.PlaceOrder(request.UserId, request.ProductId));
        }
    }
}
