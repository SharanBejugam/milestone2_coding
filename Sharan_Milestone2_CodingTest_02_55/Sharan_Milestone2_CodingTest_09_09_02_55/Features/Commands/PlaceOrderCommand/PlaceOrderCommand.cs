﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Commands.PlaceOrderCommand
{
    public class PlaceOrderCommand : IRequest<ProductOrder>
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
    }
}
