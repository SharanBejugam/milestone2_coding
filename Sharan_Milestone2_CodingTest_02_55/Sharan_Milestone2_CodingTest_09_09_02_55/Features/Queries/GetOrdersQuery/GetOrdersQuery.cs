﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using System.Collections.Generic;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetOrdersQuery
{
    public class GetOrdersQuery : IRequest<IEnumerable<ProductOrder>>
    {
        public int UserId { get; set; }
    }
}
