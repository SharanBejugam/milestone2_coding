﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using Sharan_Milestone2_CodingTest_09_09_02_55.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetOrdersQuery
{
    public class GetOrdersHandler : IRequestHandler<GetOrdersQuery, IEnumerable<ProductOrder>>
    {
        private readonly IGroceryService _data;

        public GetOrdersHandler(IGroceryService data)
        {
            _data = data;
        }

        public async Task<IEnumerable<ProductOrder>> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetOrders(request.UserId));
        }
    }
}
