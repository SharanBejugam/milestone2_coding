﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using Sharan_Milestone2_CodingTest_09_09_02_55.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetAllProductsQuery
{
    public class GetAllProductsHandler : IRequestHandler<GetAllProductsQuery, IEnumerable<Product>>
    {
        private readonly IGroceryService _data;

        public GetAllProductsHandler(IGroceryService data)
        {
            _data = data;
        }

        public async Task<IEnumerable<Product>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllProducts());
        }
    }
}
