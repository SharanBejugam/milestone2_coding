﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using Sharan_Milestone2_CodingTest_09_09_02_55.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetProductByIdQuery
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private readonly IGroceryService _data;

        public GetProductByIdHandler(IGroceryService data)
        {
            _data = data;
        }

        public async Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetProductById(request.ProductId));
        }
    }
}
