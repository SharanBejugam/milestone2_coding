﻿using MediatR;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using Sharan_Milestone2_CodingTest_09_09_02_55.Persistence;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetAllCategoriesQuery
{
    public class GetAllCategoriesHandler : IRequestHandler<GetAllCategoriesQuery, IEnumerable<Categories>>
    {
        private readonly IGroceryService _data;

        public GetAllCategoriesHandler(IGroceryService data)
        {
            _data = data;
        }

        public async Task<IEnumerable<Categories>> Handle(GetAllCategoriesQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllCategories());
        }
    }
}
