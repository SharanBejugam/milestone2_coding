﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sharan_Milestone2_CodingTest_09_09_02_55.Features.Commands.PlaceOrderCommand;
using Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetAllCategoriesQuery;
using Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetAllProductsQuery;
using Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetOrdersQuery;
using Sharan_Milestone2_CodingTest_09_09_02_55.Features.Queries.GetProductByIdQuery;
using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ISender _sender;

        public HomeController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        public async Task<IEnumerable<Categories>> GetAllCategories()
        {
            return await _sender.Send(new GetAllCategoriesQuery());
        }

        [HttpGet]
        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await _sender.Send(new GetAllProductsQuery());
        }

        [HttpGet]
        public async Task<IEnumerable<ProductOrder>> GetOrders(int userid)
        {
            return await _sender.Send(new GetOrdersQuery() { UserId = userid });
        }

        [HttpGet]
        public async Task<Product> GetProductById(int productid)
        {
            return await _sender.Send(new GetProductByIdQuery() { ProductId = productid });
        }

        [HttpPost]
        public async Task<ProductOrder> PlaceOrder(int userid, int productid)
        {
            return await _sender.Send(new PlaceOrderCommand() { ProductId = productid, UserId = userid });
        }
    }
}
