﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Models
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public long? MobileNumber { get; set; }
        public int? PinCode { get; set; }
        public string HouseNoBuldingName { get; set; }
        public string RoadArea { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
