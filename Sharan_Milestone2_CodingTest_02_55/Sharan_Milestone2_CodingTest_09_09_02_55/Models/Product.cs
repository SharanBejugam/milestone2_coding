﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public int? Stock { get; set; }
        public int? CatId { get; set; }
        public string Photo { get; set; }

        public virtual Categories Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
