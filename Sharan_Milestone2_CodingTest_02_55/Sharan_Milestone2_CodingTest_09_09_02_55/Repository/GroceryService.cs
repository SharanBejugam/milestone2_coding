﻿using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using Sharan_Milestone2_CodingTest_09_09_02_55.Persistence;
using System.Collections.Generic;
using System.Linq;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Repository
{
    public class GroceryService : IGroceryService
    {
        private readonly GroceriesDbContext _context;

        public GroceryService(GroceriesDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Categories> GetAllCategories()
        {
            return _context.Categories.ToList();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _context.Product.ToList();
        }

        public IEnumerable<ProductOrder> GetOrders(int userid)
        {
            return _context.ProductOrder.Where(x => x.UserId == userid);
        }

        public Product GetProductById(int productid)
        {
            return GetAllProducts().SingleOrDefault(x => x.ProductId == productid);
        }

        public ProductOrder PlaceOrder(int userid, int productid)
        {
            var order = new ProductOrder()
            {
                UserId = userid,
                ProductId = productid,
                OrderId = _context.ProductOrder.Max(x => x.OrderId) + 1,
                User = _context.ApplicationUser.SingleOrDefault(x => x.UserId == userid),
                Product = _context.Product.SingleOrDefault(x => x.ProductId == productid)
            };

            _context.ProductOrder.Add(order);
            _context.SaveChanges();
            return order;
        }
    }
}
