﻿using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Tests
{
    internal class GroceryServiceFake : IGroceryServiceFake
    {
        private static List<ApplicationUser> _applicationUsers;
        private static List<Product> _products;
        private static List<Categories> _categories;
        private static List<ProductOrder> _productOrders;

        public GroceryServiceFake()
        {
            _applicationUsers = new List<ApplicationUser>()
            {
                new ApplicationUser()
                {
                    UserId = 10,
                    Name = "Sharan",
                    Email = "sharan@gmail.com",
                    MobileNumber = 1234567890,
                    PinCode = 560027,
                    HouseNoBuldingName = "1-2-3,Mamatha",
                    RoadArea = "ShanthiNagar",
                    City = "Bangalore",
                    State = "Karnataka"
                },
                new ApplicationUser()
                {
                    UserId = 11,
                    Name = "Harsh",
                    Email = "harsh@gmail.com",
                    MobileNumber = 0987654321,
                    PinCode = 560027,
                    HouseNoBuldingName = "1-2-3,Mamatha",
                    RoadArea = "ShanthiNagar",
                    City = "Bangalore",
                    State = "Karnataka"
                }
            };
            _products = new List<Product>()
            {
                new Product()
                {
                    ProductId = 10,
                    ProductName = "T-Shirt",
                    Description = "High Quality",
                    Amount = 3000,
                    Stock = 10,
                    CatId = 10,
                    Photo = null
                },
                new Product()
                {
                    ProductId = 11,
                    ProductName = "Pant",
                    Description = "High Quality",
                    Amount = 5000,
                    Stock = 10,
                    CatId = 11,
                    Photo = null
                }

            };
            _categories = new List<Categories>()
            {
                new Categories()
                {
                    Catid = 10,
                    CategoryName = "T-Shirts"
                },
                new Categories()
                {
                    Catid = 11,
                    CategoryName = "Pants"
                }
            };
            _productOrders = new List<ProductOrder>()
            {
                new ProductOrder()
                {
                    OrderId = 100,
                    ProductId = 10,
                    UserId = 10
                }
            };
        }

        public IEnumerable<Categories> GetAllCategories()
        {
            return _categories;
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _products;
        }

        public IEnumerable<ProductOrder> GetOrders(int userid)
        {
            return _productOrders.Where(x => x.UserId == userid);
        }

        public Product GetProductById(int productid)
        {
            return _products.SingleOrDefault(x => x.ProductId == productid);
        }

        public ProductOrder PlaceOrder(int userid, int productid)
        {
            var order = new ProductOrder()
            {
                UserId = userid,
                ProductId = productid,
                OrderId = _productOrders.Max(x => x.OrderId) + 1,
                User = _applicationUsers.SingleOrDefault(x => x.UserId == userid),
                Product = _products.SingleOrDefault(x => x.ProductId == productid)
            };
            _productOrders.Add(order);
            return order;
        }

    }
}
