using Microsoft.AspNetCore.Mvc;
using Sharan_Milestone2_CodingTest_09_09_02_55.Controllers;
using System;
using Xunit;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Tests
{
    public class ControllerTest
    {
        private readonly HomeController _controller;
        private readonly IGroceryServiceFake _groceryService;

        public ControllerTest(HomeController controller, IGroceryServiceFake groceryService)
        {
            _controller = controller;
            _groceryService = groceryService;
        }

        [Fact]
        public void GetProductById_WhenCalled_ReturnsOkResult()
        {
            int id = 10;
            var okResult = _controller.GetProductById(10);
            Assert.Equal(id,(okResult).Id);
        }

        [Fact]
        public void GetAllCategories_WhenCalled_ReturnsResult()
        {
            var okResult = _controller.GetAllCategories();
            Assert.IsType<OkResult>(okResult);
        }
    }
}
