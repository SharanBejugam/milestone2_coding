﻿using Sharan_Milestone2_CodingTest_09_09_02_55.Models;
using System.Collections.Generic;

namespace Sharan_Milestone2_CodingTest_09_09_02_55.Tests
{
    public interface IGroceryServiceFake
    {
        IEnumerable<Categories> GetAllCategories();
        IEnumerable<Product> GetAllProducts();
        IEnumerable<ProductOrder> GetOrders(int userid);
        Product GetProductById(int productid);
        ProductOrder PlaceOrder(int userid, int productid);
    }
}